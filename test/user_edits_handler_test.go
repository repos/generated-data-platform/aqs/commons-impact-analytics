package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockUserEditsLogic struct {
	mock.Mock
}

func (m *MockUserEditsLogic) ProcessUserEditsLogic(params entities.UserEditsParameters, ctx entities.CallContext) (*problem.Problem, entities.UserEditsResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.UserEditsResponse)
}

func TestUserEditsHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	userName := "Great_editor"
	editType := "update"
	start := "2000081000"
	end := "2007092100"
	mockLogger := &log.Logger{}

	mockResponse := entities.UserEditsResponse{
		Context: entities.UserEditsContext{
			Endpoint: "commons-analytics/edits-per-user-monthly",
			UserName: userName,
			EditType: editType,
			Start:    start,
			End:      end,
		},
		Items: []entities.UserEdits{
			{
				Timestamp: "2020031500",
				EditCount: 2019,
			}},
	}

	mockLogic := new(MockUserEditsLogic)

	handler := &handlers.UserEditsHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.UserEditsParameters{
		UserName: userName,
		EditType: editType,
		Start:    start,
		End:      end,
	}
	req = mux.SetURLVars(req, map[string]string{
		"user-name": params.UserName,
		"edit-type": params.EditType,
		"start":     params.Start,
		"end":       params.End,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessUserEditsLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.UserEditsResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessUserEditsLogic", params, callContext)
}

func TestUserEditsHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockUserEditsLogic)

	handler := &handlers.UserEditsHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.UserEditsParameters{
		UserName: "Great_editor",
		EditType: "update",
		Start:    "2000081000",
		End:      "2007092100",
	}
	req = mux.SetURLVars(req, map[string]string{
		"user-name": params.UserName,
		"edit-type": params.EditType,
		"start":     params.Start,
		"end":       params.End,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessUserEditsLogic", params, callContext).
		Return(mockError, entities.UserEditsResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessUserEditsLogic", params, callContext)
}
