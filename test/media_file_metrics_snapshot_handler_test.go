package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockMediaFileMetricsSnapshotLogic struct {
	mock.Mock
}

func (m *MockMediaFileMetricsSnapshotLogic) ProcessMediaFileMetricsLogic(parameters entities.MediaFileMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFileMetricsSnapshotResponse) {
	args := m.Called(parameters, callContext)
	return args.Get(0).(*problem.Problem), args.Get(1).(entities.MediaFileMetricsSnapshotResponse)
}

func TestMediaFileMetricsHandler_HandleHTTP(t *testing.T) {
	mediafile := "sample"
	start := "2022010100"
	end := "2022010800"
	var pbm = (*problem.Problem)(nil)

	mockLogger := &log.Logger{}

	mockResponse := entities.MediaFileMetricsSnapshotResponse{
		Context: entities.MediaFileMetricsSnapshotContext{
			Endpoint:  "commons-analytics/media-file-metrics-snapshot",
			MediaFile: mediafile,
			Start:     start,
			End:       end,
		},
		Items: []entities.MediaFileMetricsSnapshot{
			{
				Timestamp:           "2022010700",
				LeveragingWikiCount: 0,
				LeveragingPageCount: 0,
			}},
	}

	mockLogic := new(MockMediaFileMetricsSnapshotLogic)

	handler := &handlers.MediafileMetricsSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022010100/2022010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022010100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"media-file": mediafile,
		"start":      start,
		"end":        end,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}
	params := entities.MediaFileMetricsSnapshotParameters{
		MediaFile: mediafile,
		Start:     start,
		End:       end,
	}

	mockLogic.On("ProcessMediaFileMetricsLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.MediaFileMetricsSnapshotResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessMediaFileMetricsLogic", params, callContext)
}

func TestMediaFileMetricsHandler_ErrorFromLogic(t *testing.T) {
	mediafile := "sample"
	start := "2022010100"
	end := "2022010800"

	mockLogger := &log.Logger{}

	mockLogic := new(MockMediaFileMetricsSnapshotLogic)

	handler := &handlers.MediafileMetricsSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022010100/2022010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022010100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"media-file": mediafile,
		"start":      start,
		"end":        end,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}
	params := entities.MediaFileMetricsSnapshotParameters{
		MediaFile: mediafile,
		Start:     start,
		End:       end,
	}
	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	mockLogic.On("ProcessMediaFileMetricsLogic", params, callContext).
		Return(mockError, entities.MediaFileMetricsSnapshotResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessMediaFileMetricsLogic", params, callContext)
}

func TestMediaFileMetricsHandler_InvalidTimestamp(t *testing.T) {
	mediafile := "sample"
	start := "12022010100"
	end := "2022010800"

	mockLogger := &log.Logger{}

	mockLogic := new(MockMediaFileMetricsSnapshotLogic)

	handler := &handlers.MediafileMetricsSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/12022010100/2022010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/12022010100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"media-file": mediafile,
		"start":      start,
		"end":        end,
	})

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUri)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}

func TestMediaFileMetricsHandler_EndBeforeSTart(t *testing.T) {
	mediafile := "sample"
	start := "2022010100"
	end := "2021010800"

	mockLogger := &log.Logger{}

	mockLogic := new(MockMediaFileMetricsSnapshotLogic)

	handler := &handlers.MediafileMetricsSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022010100/2021010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022010100/2021010800")
	req = mux.SetURLVars(req, map[string]string{
		"media-file": mediafile,
		"start":      start,
		"end":        end,
	})

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDateEndBeforeStart, reqUri)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}
