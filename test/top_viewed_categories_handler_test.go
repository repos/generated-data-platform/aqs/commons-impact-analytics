package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockTopViewedCategoriesLogic struct {
	mock.Mock
}

func (m *MockTopViewedCategoriesLogic) ProcessTopViewedCategoriesLogic(params entities.TopViewedCategoriesParameters, ctx entities.CallContext) (*problem.Problem, entities.TopViewedCategoriesResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.TopViewedCategoriesResponse)
}

func TestTopViewedCategoriesHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	categoryScope := "deep"
	wiki := "et.wikipedia"
	year := "2000"
	month := "08"
	mockLogger := &log.Logger{}

	mockResponse := entities.TopViewedCategoriesResponse{
		Context: entities.TopViewedCategoriesContext{
			Endpoint:      "commons-analytics/top-viewed-categories-monthly",
			CategoryScope: categoryScope,
			Wiki:          wiki,
			Year:          year,
			Month:         month,
		},
		Items: []entities.TopViewedCategories{
			{
				Category:      "Bodies of Water",
				PageviewCount: 42,
				Rank:          2,
			}, {
				Category:      "Bodies of Work",
				PageviewCount: 84,
				Rank:          1,
			}},
	}

	mockLogic := new(MockTopViewedCategoriesLogic)

	handler := &handlers.TopViewedCategoriesHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopViewedCategoriesParameters{
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}
	req = mux.SetURLVars(req, map[string]string{
		"category-scope": params.CategoryScope,
		"wiki":           params.Wiki,
		"year":           params.Year,
		"month":          params.Month,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopViewedCategoriesLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.TopViewedCategoriesResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessTopViewedCategoriesLogic", params, callContext)
}

func TestTopViewedCategoriesHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockTopViewedCategoriesLogic)

	handler := &handlers.TopViewedCategoriesHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopViewedCategoriesParameters{
		CategoryScope: "deep",
		Wiki:          "et.wikipedia",
		Year:          "2000",
		Month:         "08",
	}
	req = mux.SetURLVars(req, map[string]string{
		"category-scope": params.CategoryScope,
		"wiki":           params.Wiki,
		"year":           params.Year,
		"month":          params.Month,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopViewedCategoriesLogic", params, callContext).
		Return(mockError, entities.TopViewedCategoriesResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessTopViewedCategoriesLogic", params, callContext)
}
