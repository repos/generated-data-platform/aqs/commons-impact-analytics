package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockMediaFilePageviewsLogic struct {
	mock.Mock
}

func (m *MockMediaFilePageviewsLogic) ProcessMediaFilePageviewsLogic(params entities.MediaFilePageviewsParameters, ctx entities.CallContext) (*problem.Problem, entities.MediaFilePageviewsResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.MediaFilePageviewsResponse)
}

func TestMediaFilePageviewsHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)

	mockLogger := &log.Logger{}

	mediaFile := "Duck.jpg"
	wiki := "et.wikipedia"
	start := "2000081000"
	end := "2007092100"

	mockResponse := entities.MediaFilePageviewsResponse{
		Context: entities.MediaFilePageviewsContext{
			Endpoint:  "commons-analytics/pageviews-per-media-file-monthly",
			MediaFile: mediaFile,
			Wiki:      wiki,
			Start:     start,
			End:       end,
		},
		Items: []entities.MediaFilePageviews{
			{
				Timestamp:     "2020031500",
				PageviewCount: 2019,
			}},
	}

	mockLogic := new(MockMediaFilePageviewsLogic)

	handler := &handlers.MediaFilePageviewsHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.MediaFilePageviewsParameters{
		MediaFile: mediaFile,
		Wiki:      wiki,
		Start:     start,
		End:       end,
	}
	req = mux.SetURLVars(req, map[string]string{
		"media-file": params.MediaFile,
		"wiki":       params.Wiki,
		"start":      params.Start,
		"end":        params.End,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessMediaFilePageviewsLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.MediaFilePageviewsResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessMediaFilePageviewsLogic", params, callContext)
}

func TestMediaFilePageviewsHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockMediaFilePageviewsLogic)

	handler := &handlers.MediaFilePageviewsHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.MediaFilePageviewsParameters{
		MediaFile: "Duck.jpg",
		Wiki:      "et.wikipedia",
		Start:     "2000081000",
		End:       "2007092100",
	}
	req = mux.SetURLVars(req, map[string]string{
		"media-file": params.MediaFile,
		"wiki":       params.Wiki,
		"start":      params.Start,
		"end":        params.End,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessMediaFilePageviewsLogic", params, callContext).
		Return(mockError, entities.MediaFilePageviewsResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessMediaFilePageviewsLogic", params, callContext)
}
