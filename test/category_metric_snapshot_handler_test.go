package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockCategoryMetricLogic struct {
	mock.Mock
}

func (m *MockCategoryMetricLogic) ProcessCategoryMetricLogic(parameters entities.CategoryMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryMetricResponse) {
	args := m.Called(parameters, callContext)
	return args.Get(0).(*problem.Problem), args.Get(1).(entities.CategoryMetricResponse)
}

func TestCategoryMetricSnapshotHandler_HandleHTTP(t *testing.T) {
	category := "sample"
	start := "2022010100"
	end := "2022010800"
	var pbm = (*problem.Problem)(nil)

	mockLogger := &log.Logger{}

	mockResponse := entities.CategoryMetricResponse{
		Context: entities.CategoryMetricsSnapshotContext{
			Endpoint: "commons-analytics/category-metrics-snapshot",
			Category: category,
			Start:    start,
			End:      end,
		},
		Items: []entities.CategoryMetric{
			{
				MediaFileCount:          0,
				MediaFileCountDeep:      0,
				UsedMediaFileCount:      0,
				UsedMediaFileCountDeep:  0,
				LeveragingWikiCount:     0,
				LeveragingWikiCountDeep: 0,
				LeveragingPageCount:     0,
				LeveragingPageCountDeep: 0,
			}},
	}

	mockLogic := new(MockCategoryMetricLogic)

	handler := &handlers.CategoryMetricSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022010100/2022010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022010100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"category": category,
		"start":    start,
		"end":      end,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}
	params := entities.CategoryMetricsSnapshotParameters{
		Category: category,
		Start:    start,
		End:      end,
	}

	mockLogic.On("ProcessCategoryMetricLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.CategoryMetricResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessCategoryMetricLogic", params, callContext)
}

func TestCategoryMetricSnapshotHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	category := "sample"
	start := "2022010100"
	end := "2022010800"

	mockLogger := &log.Logger{}

	mockLogic := new(MockCategoryMetricLogic)

	handler := &handlers.CategoryMetricSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022010100/2022010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022010100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"category": category,
		"start":    start,
		"end":      end,
	})
	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}
	params := entities.CategoryMetricsSnapshotParameters{
		Category: category,
		Start:    start,
		End:      end,
	}

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	mockLogic.On("ProcessCategoryMetricLogic", params, callContext).
		Return(mockError, entities.CategoryMetricResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessCategoryMetricLogic", params, callContext)
}

func TestCategoryMetricSnapshotHandler_InvalidTimestamp(t *testing.T) {
	category := "sample"
	start := "2022210100"
	end := "2022010800"

	mockLogger := &log.Logger{}

	mockLogic := new(MockCategoryMetricLogic)

	handler := &handlers.CategoryMetricSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022210100/2022010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022210100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"category": category,
		"start":    start,
		"end":      end,
	})

	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}

func TestCategoryMetricSnapshotHandler_StartBeforeEnd(t *testing.T) {
	category := "sample"
	start := "2022010100"
	end := "2021010800"

	mockLogger := &log.Logger{}

	mockLogic := new(MockCategoryMetricLogic)

	handler := &handlers.CategoryMetricSnapshotHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/sample/2022010100/2021010800"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/sample/2022010100/2022010800")
	req = mux.SetURLVars(req, map[string]string{
		"category": category,
		"start":    start,
		"end":      end,
	})

	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDateEndBeforeStart, reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}
