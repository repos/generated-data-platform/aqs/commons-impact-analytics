package test

import (
	"fmt"
	"os"
	"strings"
)

const remoteAddr = "127.0.0.1:8080"

func testURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/commons-analytics"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}
