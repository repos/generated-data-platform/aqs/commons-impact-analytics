package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockCategoryPageviewsLogic struct {
	mock.Mock
}

func (m *MockCategoryPageviewsLogic) ProcessCategoryPageviewsLogic(params entities.CategoryPageviewsParameters, ctx entities.CallContext) (*problem.Problem, entities.CategoryPageviewsResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.CategoryPageviewsResponse)
}

func TestCategoryPageviewsHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	category := "Library of Congress"
	categoryScope := "deep"
	wiki := "et.wikipedia"
	start := "2000081000"
	end := "2007092100"
	mockLogger := &log.Logger{}

	mockResponse := entities.CategoryPageviewsResponse{
		Context: entities.CategoryPageviewsContext{
			Endpoint:      "commons-analytics/pageviews-per-category-monthly",
			Category:      category,
			CategoryScope: categoryScope,
			Wiki:          wiki,
			Start:         start,
			End:           end,
		},
		Items: []entities.CategoryPageviews{
			{
				Timestamp:     "2020031500",
				PageviewCount: 2019,
			}},
	}

	mockLogic := new(MockCategoryPageviewsLogic)

	handler := &handlers.CategoryPageviewsHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.CategoryPageviewsParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Start:         start,
		End:           end,
	}
	req = mux.SetURLVars(req, map[string]string{
		"category":       params.Category,
		"category-scope": params.CategoryScope,
		"wiki":           params.Wiki,
		"start":          params.Start,
		"end":            params.End,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessCategoryPageviewsLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.CategoryPageviewsResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessCategoryPageviewsLogic", params, callContext)
}

func TestCategoryPageviewsHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockCategoryPageviewsLogic)

	handler := &handlers.CategoryPageviewsHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.CategoryPageviewsParameters{
		Category:      "Library of Congress",
		CategoryScope: "deep",
		Wiki:          "et.wikipedia",
		Start:         "2000081000",
		End:           "2007092100",
	}
	req = mux.SetURLVars(req, map[string]string{
		"category":       params.Category,
		"category-scope": params.CategoryScope,
		"wiki":           params.Wiki,
		"start":          params.Start,
		"end":            params.End,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessCategoryPageviewsLogic", params, callContext).
		Return(mockError, entities.CategoryPageviewsResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessCategoryPageviewsLogic", params, callContext)
}
