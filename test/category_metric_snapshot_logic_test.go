package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

// MockDataGateway is a mock implementation of the DataGateway interface
type MockDataGateway struct {
	mock.Mock
}

func (m *MockDataGateway) CallDataGateway(uri, apiURL string, pathParams ...string) ([]byte, error) {
	args := m.Called(uri, apiURL, pathParams)
	err := args.Error(1)
	if err != nil {
		return nil, err
	}
	return args.Get(0).([]byte), args.Error(1)
}

func TestProcessCategoryMetricLogic(t *testing.T) {
	mockDataGateway := new(MockDataGateway)
	categoryMetricLogic := logic.NewCategoryMetricLogic(&configuration.Config{
		DataGatewayURI: "http://example.com"}, mockDataGateway)
	req, err := http.NewRequest("GET", "localhost:8080/metrics/commons-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	// Successful response
	t.Run("successful response", func(t *testing.T) {
		mockResponse := entities.DataGatewayCategoryMetricResponse{
			Rows: []entities.CategoryMetric{
				{
					Timestamp:               "2023-11-01 00:00:00.000Z",
					MediaFileCount:          1,
					MediaFileCountDeep:      0,
					UsedMediaFileCount:      1,
					UsedMediaFileCountDeep:  0,
					LeveragingWikiCount:     1,
					LeveragingWikiCountDeep: 0,
					LeveragingPageCount:     3,
					LeveragingPageCountDeep: 0,
				},
			},
		}

		mockData, err := json.Marshal(mockResponse)
		require.NoError(t, err)

		mockDataGateway.On("CallDataGateway", "http://example.com", "category_metrics_snapshot", mock.Anything).Return(mockData, nil)

		parameters := entities.CategoryMetricsSnapshotParameters{
			Category: "test-category",
			Start:    "20220101",
			End:      "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
		}

		problem, response := categoryMetricLogic.ProcessCategoryMetricLogic(parameters, callContext)

		assert.Nil(t, problem)
		assert.Equal(t, 1, len(response.Items))
		assert.Equal(t, mockResponse.Rows[0].Timestamp, response.Items[0].Timestamp)
		assert.Equal(t, mockResponse.Rows[0].MediaFileCount, response.Items[0].MediaFileCount)
		assert.Equal(t, mockResponse.Rows[0].LeveragingWikiCount, response.Items[0].LeveragingWikiCount)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})
	// CallDataGateway returns an error
	t.Run("CallDataGateway returns an error", func(t *testing.T) {
		errMessage := `{"detail":"Unable to parse timestamp: 2023-12-01T00:00:00Z1 (parsing time \"2023-12-01T00:00:00Z1\": extra text: \"1\")","status":400,"title":"Invalid RFC3339 timestamp"}`
		mockDataGateway.On("CallDataGateway", "http://example.com", "category_metrics_snapshot", mock.Anything).Return(nil, errors.New(errMessage))

		parameters := entities.CategoryMetricsSnapshotParameters{
			Category: "test-category",
			Start:    "2023-12-01T00:00:00Z1",
			End:      "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
			Logger:     rLogger,
		}

		problem, response := categoryMetricLogic.ProcessCategoryMetricLogic(parameters, callContext)

		assert.NotNil(t, problem)
		assert.Empty(t, response.Items)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})

	// JSON unmarshalling fails
	t.Run("JSON unmarshalling fails", func(t *testing.T) {
		invalidJSON := []byte(`{invalid-json}`)
		mockDataGateway.On("CallDataGateway", "http://example.com", "category_metrics_snapshot", mock.Anything).Return(invalidJSON, nil)

		parameters := entities.CategoryMetricsSnapshotParameters{
			Category: "test-category",
			Start:    "20220101",
			End:      "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
			Logger:     rLogger,
		}

		problem, response := categoryMetricLogic.ProcessCategoryMetricLogic(parameters, callContext)

		assert.NotNil(t, problem)
		assert.Empty(t, response.Items)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})

	// dataGatewayResponse.Rows is empty
	t.Run("dataGatewayResponse.Rows is empty", func(t *testing.T) {
		mockResponse := entities.DataGatewayCategoryMetricResponse{Rows: []entities.CategoryMetric{}}
		mockData, err := json.Marshal(mockResponse)
		require.NoError(t, err)

		mockDataGateway.On("CallDataGateway", "http://example.com", "category_metrics_snapshot", mock.Anything).Return(mockData, nil)

		parameters := entities.CategoryMetricsSnapshotParameters{
			Category: "test-category",
			Start:    "20220101",
			End:      "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
		}

		problem, response := categoryMetricLogic.ProcessCategoryMetricLogic(parameters, callContext)

		assert.Equal(t, aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL).Error(), problem.Error())
		assert.Empty(t, response.Items)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})
}
