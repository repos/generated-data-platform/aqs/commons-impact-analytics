package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

func TestProcessCategoryPageviewsLogic(t *testing.T) {
	mockDataGateway := new(MockDataGateway)
	categoryPageviewsLogic := logic.NewCategoryPageviewsLogic(&configuration.Config{
		DataGatewayURI: "http://example.com"}, mockDataGateway)
	req, err := http.NewRequest("GET", "localhost:8080/metrics/commons-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	// Successful response
	t.Run("successful response", func(t *testing.T) {
		mockResponse := entities.DataGatewayCategoryPageviewsResponse{
			Rows: []entities.CategoryPageviews{
				{
					Timestamp:     "2023110100",
					PageviewCount: 1,
				},
			},
		}

		mockData, err := json.Marshal(mockResponse)
		require.NoError(t, err)

		mockDataGateway.On("CallDataGateway", "http://example.com", "pageviews_per_category_monthly", mock.Anything).Return(mockData, nil)

		parameters := entities.CategoryPageviewsParameters{
			Category:      "test-category",
			CategoryScope: "test-category",
			Wiki:          "en.wikipedia",
			Start:         "20220101",
			End:           "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
		}

		problem, response := categoryPageviewsLogic.ProcessCategoryPageviewsLogic(parameters, callContext)

		assert.Nil(t, problem)
		assert.Equal(t, 1, len(response.Items))
		assert.Equal(t, mockResponse.Rows[0].Timestamp, response.Items[0].Timestamp)
		assert.Equal(t, mockResponse.Rows[0].PageviewCount, response.Items[0].PageviewCount)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})
	// CallDataGateway returns an error
	t.Run("CallDataGateway returns an error", func(t *testing.T) {
		errMessage := `{"detail":"Unable to parse timestamp: 2023-12-01T00:00:00Z1 (parsing time \"2023-12-01T00:00:00Z1\": extra text: \"1\")","status":400,"title":"Invalid RFC3339 timestamp"}`
		mockDataGateway.On("CallDataGateway", "http://example.com", "pageviews_per_category_monthly", mock.Anything).Return(nil, errors.New(errMessage))

		parameters := entities.CategoryPageviewsParameters{
			Category:      "test-category",
			CategoryScope: "test-category",
			Wiki:          "en.wikipedia",
			Start:         "2023-12-01T00:00:00Z1",
			End:           "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
			Logger:     rLogger,
		}

		problem, response := categoryPageviewsLogic.ProcessCategoryPageviewsLogic(parameters, callContext)

		assert.NotNil(t, problem)
		assert.Empty(t, response.Items)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})

	// JSON unmarshalling fails
	t.Run("JSON unmarshalling fails", func(t *testing.T) {
		invalidJSON := []byte(`{invalid-json}`)
		mockDataGateway.On("CallDataGateway", "http://example.com", "pageviews_per_category_monthly", mock.Anything).Return(invalidJSON, nil)

		parameters := entities.CategoryPageviewsParameters{
			Category:      "test-category",
			CategoryScope: "test-category",
			Wiki:          "en.wikipedia",
			Start:         "20220101",
			End:           "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
			Logger:     rLogger,
		}

		problem, response := categoryPageviewsLogic.ProcessCategoryPageviewsLogic(parameters, callContext)

		assert.NotNil(t, problem)
		assert.Empty(t, response.Items)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})

	// dataGatewayResponse.Rows is empty
	t.Run("dataGatewayResponse.Rows is empty", func(t *testing.T) {
		mockResponse := entities.DataGatewayCategoryPageviewsResponse{Rows: []entities.CategoryPageviews{}}
		mockData, err := json.Marshal(mockResponse)
		require.NoError(t, err)

		mockDataGateway.On("CallDataGateway", "http://example.com", "pageviews_per_category_monthly", mock.Anything).Return(mockData, nil)

		parameters := entities.CategoryPageviewsParameters{
			Category:      "test-category",
			CategoryScope: "test-category",
			Wiki:          "en.wikipedia",
			Start:         "20220101",
			End:           "20220131",
		}

		callContext := entities.CallContext{
			RequestURL: "http://example.com/request",
		}

		problem, response := categoryPageviewsLogic.ProcessCategoryPageviewsLogic(parameters, callContext)

		assert.Equal(t, aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL).Error(), problem.Error())
		assert.Empty(t, response.Items)

		mockDataGateway.AssertExpectations(t)
		mockDataGateway.ExpectedCalls = nil

	})
}
