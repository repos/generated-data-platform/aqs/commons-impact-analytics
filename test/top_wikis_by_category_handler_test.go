package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockTopWikisByCategoryLogic struct {
	mock.Mock
}

func (m *MockTopWikisByCategoryLogic) ProcessTopWikisByCategoryLogic(params entities.TopWikisByCategoryParameters, ctx entities.CallContext) (*problem.Problem, entities.TopWikisByCategoryResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.TopWikisByCategoryResponse)
}

func TestTopWikisByCategoryHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	category := "Library of Congress"
	categoryScope := "shallow"
	year := "2000"
	month := "08"
	mockLogger := &log.Logger{}

	mockResponse := entities.TopWikisByCategoryResponse{
		Context: entities.TopWikisByCategoryContext{
			Endpoint:      "commons-analytics/top-wikis-per-category-monthly",
			Category:      category,
			CategoryScope: categoryScope,
			Year:          year,
			Month:         month,
		},
		Items: []entities.TopWikisByCategory{
			{
				Wiki:          "et.wikipedia",
				PageviewCount: 42,
				Rank:          2,
			}, {
				Wiki:          "ro.wikipedia",
				PageviewCount: 8,
				Rank:          3,
			}},
	}

	mockLogic := new(MockTopWikisByCategoryLogic)

	handler := &handlers.TopWikisByCategoryHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopWikisByCategoryParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Year:          year,
		Month:         month,
	}
	req = mux.SetURLVars(req, map[string]string{
		"category":       params.Category,
		"category-scope": params.CategoryScope,
		"year":           params.Year,
		"month":          params.Month,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopWikisByCategoryLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.TopWikisByCategoryResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessTopWikisByCategoryLogic", params, callContext)
}

func TestTopWikisByCategoryHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockTopWikisByCategoryLogic)

	handler := &handlers.TopWikisByCategoryHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopWikisByCategoryParameters{
		Category:      "Library of Congress",
		CategoryScope: "deep",
		Year:          "2000",
		Month:         "08",
	}
	req = mux.SetURLVars(req, map[string]string{
		"category":       params.Category,
		"category-scope": params.CategoryScope,
		"year":           params.Year,
		"month":          params.Month,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopWikisByCategoryLogic", params, callContext).
		Return(mockError, entities.TopWikisByCategoryResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessTopWikisByCategoryLogic", params, callContext)
}
