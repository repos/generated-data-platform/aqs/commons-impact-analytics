package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockTopPagesByMediaFileLogic struct {
	mock.Mock
}

func (m *MockTopPagesByMediaFileLogic) ProcessTopPagesByMediaFileLogic(params entities.TopPagesByMediaFileParameters, ctx entities.CallContext) (*problem.Problem, entities.TopPagesByMediaFileResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.TopPagesByMediaFileResponse)
}

func TestTopPagesByMediaFileHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	mediaFile := "Duck.jpg"
	wiki := "et.wikipedia"
	year := "2000"
	month := "08"

	mockLogger := &log.Logger{}

	mockResponse := entities.TopPagesByMediaFileResponse{
		Context: entities.TopPagesByMediaFileContext{
			Endpoint:  "commons-analytics/top-pages-per-media-file-monthly",
			MediaFile: mediaFile,
			Wiki:      wiki,
			Year:      year,
			Month:     month,
		},
		Items: []entities.TopPagesByMediaFile{
			{
				PageTitle:     "Lake",
				PageviewCount: 42,
				Rank:          2,
			}, {
				PageTitle:     "Ocean",
				PageviewCount: 84,
				Rank:          1,
			}},
	}

	mockLogic := new(MockTopPagesByMediaFileLogic)

	handler := &handlers.TopPagesByMediaFileHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopPagesByMediaFileParameters{
		MediaFile: "Duck.jpg",
		Wiki:      "et.wikipedia",
		Year:      "2000",
		Month:     "08",
	}
	req = mux.SetURLVars(req, map[string]string{
		"media-file": params.MediaFile,
		"wiki":       params.Wiki,
		"year":       params.Year,
		"month":      params.Month,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopPagesByMediaFileLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.TopPagesByMediaFileResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessTopPagesByMediaFileLogic", params, callContext)
}

func TestTopPagesByMediaFileHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockTopPagesByMediaFileLogic)

	handler := &handlers.TopPagesByMediaFileHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopPagesByMediaFileParameters{
		MediaFile: "Duck.jpg",
		Wiki:      "et.wikipedia",
		Year:      "2000",
		Month:     "08",
	}
	req = mux.SetURLVars(req, map[string]string{
		"media-file": params.MediaFile,
		"wiki":       params.Wiki,
		"year":       params.Year,
		"month":      params.Month,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopPagesByMediaFileLogic", params, callContext).
		Return(mockError, entities.TopPagesByMediaFileResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessTopPagesByMediaFileLogic", params, callContext)
}
