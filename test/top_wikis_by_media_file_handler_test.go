package test

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockTopWikisByMediaFileLogic struct {
	mock.Mock
}

func (m *MockTopWikisByMediaFileLogic) ProcessTopWikisByMediaFileLogic(params entities.TopWikisByMediaFileParameters, ctx entities.CallContext) (*problem.Problem, entities.TopWikisByMediaFileResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.TopWikisByMediaFileResponse)
}

func TestTopWikisByMediaFileHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	mediaFile := "Duck.jpg"
	year := "2000"
	month := "08"
	mockLogger := &log.Logger{}

	mockResponse := entities.TopWikisByMediaFileResponse{
		Context: entities.TopWikisByMediaFileContext{
			Endpoint:  "commons-analytics/top-wikis-per-media-file-monthly",
			MediaFile: mediaFile,
			Year:      year,
			Month:     month,
		},
		Items: []entities.TopWikisByMediaFile{
			{
				Wiki:          "et.wikipedia",
				PageviewCount: 42,
				Rank:          2,
			}, {
				Wiki:          "ro.wikipedia",
				PageviewCount: 8,
				Rank:          3,
			}},
	}

	mockLogic := new(MockTopWikisByMediaFileLogic)

	handler := &handlers.TopWikisByMediaFileHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopWikisByMediaFileParameters{
		MediaFile: mediaFile,
		Year:      year,
		Month:     month,
	}
	req = mux.SetURLVars(req, map[string]string{
		"media-file": params.MediaFile,
		"year":       params.Year,
		"month":      params.Month,
	})

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopWikisByMediaFileLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.TopWikisByMediaFileResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessTopWikisByMediaFileLogic", params, callContext)
}

func TestTopWikisByMediaFileHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}

	mockLogic := new(MockTopWikisByMediaFileLogic)

	handler := &handlers.TopWikisByMediaFileHandler{
		Logger: mockLogger,
		Logic:  mockLogic,
		Config: &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.TopWikisByMediaFileParameters{
		MediaFile: "Duck.jpg",
		Year:      "2000",
		Month:     "08",
	}
	req = mux.SetURLVars(req, map[string]string{
		"media-file": params.MediaFile,
		"year":       params.Year,
		"month":      params.Month,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context: ctx,

		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessTopWikisByMediaFileLogic", params, callContext).
		Return(mockError, entities.TopWikisByMediaFileResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessTopWikisByMediaFileLogic", params, callContext)
}
