# Commons Impact metrics API

The Commons Impact Metrics API is a collection of endpoints designed to provide insight on the impact of Community contributions to Commons. So far, the data is focused on media files uploaded by- and categories belonging to GLAM actors (affiliates, projects, individual contributors etc.).

# Project structure

In order to increase maintainibility and decrease tight coupling, the project is structured within the following layers
entities -> This contains the main resources for which the API is developed.They are the least likely to change when something external changes. For example, you would not expect your category-edits entity to change just because you want to show details according to browsers.
Mainly contains struct within the go file.
logic -> This contains the main functional and processing logic for each entity in use. depends on the entities. change in the entities will require a change in the use case layer, but changes to other layers won’t.
test -> This directory contains test around each handler function.
Complete documentation can be found on [Wikitech][wikitech].

## Build, Test, & Run

You will need:

- Go
- Make

Build:

```sh-session
$ make
[...]
$
```

Test ([requires Cassandra][dockerized_test_environment]):

```sh-session
$ make CASSANDRA_HOST=xxx.xx.xxx.xx test
[...]
$
```

Run ([also requires Cassandra][dockerized_test_environment]):

```sh-session
$ ./commons-analytucs
[...]
```

## Prometheus metrics

A resource serving Prometheus request count and latency metrics can be found at `/admin/metrics`.


## API documentation

To generate and view the API documentation, read the [docs on Wikitech][wikipage].

[wikitech]: https://wikitech.wikimedia.org/wiki/Commons_Impact_Metrics
[docker_compose]: https://docs.docker.com/compose/
[wikipage]: https://wikitech.wikimedia.org/wiki/AQS_2.0#API_documentation
[dockerized_test_environment]: https://gitlab.wikimedia.org/frankie/aqs-docker-test-env
