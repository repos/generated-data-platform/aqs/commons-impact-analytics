package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopWikisByMediaFileLogicInterface interface {
	ProcessTopWikisByMediaFileLogic(parameters entities.TopWikisByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByMediaFileResponse)
}

type TopWikisByMediaFileLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewTopWikisByMediaFileLogic(config *configuration.Config, dataGateway DataGateway) TopWikisByMediaFileLogic {
	return TopWikisByMediaFileLogic{Config: config, DataGateway: dataGateway}
}

func (s TopWikisByMediaFileLogic) ProcessTopWikisByMediaFileLogic(parameters entities.TopWikisByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByMediaFileResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopWikisByMediaFileResponse{Items: make([]entities.TopWikisByMediaFile, 0)}

	pathParams := []string{parameters.MediaFile, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_wikis_per_media_file_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopWikisByMediaFileResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopWikisByMediaFileResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopWikisByMediaFileResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopWikisByMediaFileResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
