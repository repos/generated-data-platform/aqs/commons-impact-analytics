package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type CategoryMetricLogicInterface interface {
	ProcessCategoryMetricLogic(parameters entities.CategoryMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryMetricResponse)
}

type CategoryMetricLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewCategoryMetricLogic(config *configuration.Config, dataGateway DataGateway) CategoryMetricLogic {
	return CategoryMetricLogic{Config: config, DataGateway: dataGateway}
}

type CallDataGatewayFunc func(uri, apiURL string, pathParams ...string) ([]byte, error)

func (s CategoryMetricLogic) ProcessCategoryMetricLogic(parameters entities.CategoryMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryMetricResponse) {
	var err error
	var problemData *problem.Problem

	var response = entities.CategoryMetricResponse{Items: make([]entities.CategoryMetric, 0)}
	pathParams := []string{parameters.Category, convertToISO8601(parameters.Start), convertToISO8601(parameters.End)}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "category_metrics_snapshot", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryMetricResponse{}
	}

	var dataGatewayResponse entities.DataGatewayCategoryMetricResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryMetricResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.CategoryMetricResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return problemData, response
}
