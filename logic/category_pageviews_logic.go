package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type CategoryPageviewsLogicInterface interface {
	ProcessCategoryPageviewsLogic(parameters entities.CategoryPageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryPageviewsResponse)
}

type CategoryPageviewsLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewCategoryPageviewsLogic(config *configuration.Config, dataGateway DataGateway) CategoryPageviewsLogic {
	return CategoryPageviewsLogic{Config: config, DataGateway: dataGateway}
}

func (s CategoryPageviewsLogic) ProcessCategoryPageviewsLogic(parameters entities.CategoryPageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryPageviewsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.CategoryPageviewsResponse{Items: make([]entities.CategoryPageviews, 0)}

	pathParams := []string{parameters.Category, parameters.CategoryScope, parameters.Wiki, convertToISO8601(parameters.Start), convertToISO8601(parameters.End)}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "pageviews_per_category_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryPageviewsResponse{}
	}

	var dataGatewayResponse entities.DataGatewayCategoryPageviewsResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryPageviewsResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.CategoryPageviewsResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return problemData, response
}
