package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopWikisByCategoryLogicInterface interface {
	ProcessTopWikisByCategoryLogic(parameters entities.TopWikisByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByCategoryResponse)
}

type TopWikisByCategoryLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewTopWikisByCategoryLogic(config *configuration.Config, dataGateway DataGateway) TopWikisByCategoryLogic {
	return TopWikisByCategoryLogic{Config: config, DataGateway: dataGateway}
}

func (s TopWikisByCategoryLogic) ProcessTopWikisByCategoryLogic(parameters entities.TopWikisByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByCategoryResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopWikisByCategoryResponse{Items: make([]entities.TopWikisByCategory, 0)}

	pathParams := []string{parameters.Category, parameters.CategoryScope, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_wikis_per_category_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopWikisByCategoryResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopWikisByCategoryResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopWikisByCategoryResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopWikisByCategoryResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
