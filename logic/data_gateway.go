package logic

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

type DataGateway interface {
	CallDataGateway(uri, apiURL string, pathParams ...string) ([]byte, error)
}

// Default implementation of DataGateway
type DefaultDataGateway struct{}

func (d DefaultDataGateway) CallDataGateway(dataGatewayURI, apiURL string, pathParams ...string) ([]byte, error) {

	var err error

	fullURL := fmt.Sprintf("%s/%s/%s", dataGatewayURI, apiURL, strings.Join(pathParams, "/"))

	resp, err := http.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("error fetching data from URL: %v", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(io.Reader(resp.Body))

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error: received status code %d for URL %s", resp.StatusCode, fullURL)
	}
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %v", err)
	}

	return body, nil
}

func convertToISO8601(input string) string {
	const inputLayout = "2006010215"
	const outputLayout = "2006-01-02T15:00:00Z"
	t, _ := time.Parse(inputLayout, input)
	output := t.Format(outputLayout)
	return output
}
