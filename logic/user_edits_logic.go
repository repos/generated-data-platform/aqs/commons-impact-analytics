package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type UserEditsLogicInterface interface {
	ProcessUserEditsLogic(parameters entities.UserEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.UserEditsResponse)
}

type UserEditsLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewUserEditsLogic(config *configuration.Config, dataGateway DataGateway) UserEditsLogic {
	return UserEditsLogic{Config: config, DataGateway: dataGateway}
}

func (s UserEditsLogic) ProcessUserEditsLogic(parameters entities.UserEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.UserEditsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.UserEditsResponse{Items: make([]entities.UserEdits, 0)}

	pathParams := []string{parameters.UserName, parameters.EditType, convertToISO8601(parameters.Start), convertToISO8601(parameters.End)}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "edits_per_user_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.UserEditsResponse{}
	}

	var dataGatewayResponse entities.DataGatewayUserEditsResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.UserEditsResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateUserNameNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.UserEditsResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return problemData, response
}
