package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type CategoryEditsLogicInterface interface {
	ProcessCategoryEditsLogic(parameters entities.CategoryEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryEditsResponse)
}

func NewCategoryEditsLogic(config *configuration.Config, datagateway DataGateway) CategoryEditsLogic {
	return CategoryEditsLogic{Config: config, DataGateway: datagateway}
}

type CategoryEditsLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func (s CategoryEditsLogic) ProcessCategoryEditsLogic(parameters entities.CategoryEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryEditsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.CategoryEditsResponse{Items: make([]entities.CategoryEdits, 0)}
	pathParams := []string{parameters.Category, parameters.CategoryScope, parameters.EditType, convertToISO8601(parameters.Start), convertToISO8601(parameters.End)}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "edits_per_category_monthly", pathParams...)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryEditsResponse{}
	}

	var dataGatewayResponse entities.DataGatewayCategoryEditsResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryEditsResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.CategoryEditsResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return problemData, response
}
