package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopPagesByCategoryLogicInterface interface {
	ProcessTopPagesByCategoryLogic(parameters entities.TopPagesByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByCategoryResponse)
}

func NewTopPagesByCategoryLogic(config *configuration.Config, dataGateway DataGateway) TopPagesByCategoryLogic {
	return TopPagesByCategoryLogic{Config: config, DataGateway: dataGateway}
}

type TopPagesByCategoryLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func (s TopPagesByCategoryLogic) ProcessTopPagesByCategoryLogic(parameters entities.TopPagesByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByCategoryResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopPagesByCategoryResponse{Items: make([]entities.TopPagesByCategory, 0)}

	pathParams := []string{parameters.Category, parameters.CategoryScope, parameters.Wiki, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_pages_per_category_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopPagesByCategoryResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopPagesByCategoryResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopPagesByCategoryResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopPagesByCategoryResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
