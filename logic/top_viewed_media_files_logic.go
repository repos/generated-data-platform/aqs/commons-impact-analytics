package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopViewedMediaFilesLogicInterface interface {
	ProcessTopViewedMediaFilesLogic(parameters entities.TopViewedMediaFilesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedMediaFilesResponse)
}

type TopViewedMediaFilesLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewTopViewedMediaFilesLogic(config *configuration.Config, dataGateway DataGateway) TopViewedMediaFilesLogic {
	return TopViewedMediaFilesLogic{Config: config, DataGateway: dataGateway}
}

func (s TopViewedMediaFilesLogic) ProcessTopViewedMediaFilesLogic(parameters entities.TopViewedMediaFilesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedMediaFilesResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopViewedMediaFilesResponse{Items: make([]entities.TopViewedMediaFiles, 0)}

	pathParams := []string{parameters.Category, parameters.CategoryScope, parameters.Wiki, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_viewed_media_files_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopViewedMediaFilesResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopViewedMediaFilesResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopViewedMediaFilesResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopViewedMediaFilesResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
