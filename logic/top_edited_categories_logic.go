package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopEditedCategoriesLogicInterface interface {
	ProcessTopEditedCategoriesLogic(parameters entities.TopEditedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditedCategoriesResponse)
}

type TopEditedCategoriesLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewTopEditedCategoriesLogic(config *configuration.Config, dataGateway DataGateway) TopEditedCategoriesLogic {
	return TopEditedCategoriesLogic{Config: config, DataGateway: dataGateway}
}

func (s TopEditedCategoriesLogic) ProcessTopEditedCategoriesLogic(parameters entities.TopEditedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditedCategoriesResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopEditedCategoriesResponse{Items: make([]entities.TopEditedCategories, 0)}

	pathParams := []string{parameters.CategoryScope, parameters.EditType, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_edited_categories_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopEditedCategoriesResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopEditedCategoriesResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopEditedCategoriesResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopEditedCategoriesResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
