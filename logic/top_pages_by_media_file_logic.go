package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopPagesByMediaFileLogicInterface interface {
	ProcessTopPagesByMediaFileLogic(parameters entities.TopPagesByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByMediaFileResponse)
}

type TopPagesByMediaFileLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewTopPagesByMediaFileLogic(config *configuration.Config, dataGateway DataGateway) TopPagesByMediaFileLogic {
	return TopPagesByMediaFileLogic{Config: config, DataGateway: dataGateway}
}

func (s TopPagesByMediaFileLogic) ProcessTopPagesByMediaFileLogic(parameters entities.TopPagesByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByMediaFileResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopPagesByMediaFileResponse{Items: make([]entities.TopPagesByMediaFile, 0)}

	pathParams := []string{parameters.MediaFile, parameters.Wiki, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_pages_per_media_file_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopPagesByMediaFileResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopPagesByMediaFileResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopPagesByMediaFileResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopPagesByMediaFileResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
