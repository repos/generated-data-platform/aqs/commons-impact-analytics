package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopEditorsLogicInterface interface {
	ProcessTopEditorsLogic(parameters entities.TopEditorsParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditorsResponse)
}

type TopEditorsLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewTopEditorsLogic(config *configuration.Config, dataGateway DataGateway) TopEditorsLogic {
	return TopEditorsLogic{Config: config, DataGateway: dataGateway}
}

func (s TopEditorsLogic) ProcessTopEditorsLogic(parameters entities.TopEditorsParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditorsResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopEditorsResponse{Items: make([]entities.TopEditors, 0)}

	pathParams := []string{parameters.Category, parameters.CategoryScope, parameters.EditType, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_editors_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopEditorsResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopEditorsResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopEditorsResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopEditorsResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
