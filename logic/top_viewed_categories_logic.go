package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopViewedCategoriesLogicInterface interface {
	ProcessTopViewedCategoriesLogic(parameters entities.TopViewedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedCategoriesResponse)
}

func NewTopViewedCategoriesLogic(config *configuration.Config, dataGateway DataGateway) TopViewedCategoriesLogic {
	return TopViewedCategoriesLogic{Config: config, DataGateway: dataGateway}
}

type TopViewedCategoriesLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func (s TopViewedCategoriesLogic) ProcessTopViewedCategoriesLogic(parameters entities.TopViewedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedCategoriesResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopViewedCategoriesResponse{Items: make([]entities.TopViewedCategories, 0)}

	pathParams := []string{parameters.CategoryScope, parameters.Wiki, parameters.Year, parameters.Month}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "top_viewed_categories_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopViewedCategoriesResponse{}
	}

	var dataGatewayResponse entities.DataGatewayTopViewedCategoriesResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.TopViewedCategoriesResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateWikiNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopViewedCategoriesResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return noProblem, response
}
