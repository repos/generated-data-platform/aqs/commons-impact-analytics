package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MediaFilePageviewsLogicInterface interface {
	ProcessMediaFilePageviewsLogic(parameters entities.MediaFilePageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFilePageviewsResponse)
}

type MediaFilePageviewsLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewMediaFilePageviewsLogic(config *configuration.Config, dataGateway DataGateway) MediaFilePageviewsLogic {
	return MediaFilePageviewsLogic{Config: config, DataGateway: dataGateway}
}

func (s MediaFilePageviewsLogic) ProcessMediaFilePageviewsLogic(parameters entities.MediaFilePageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFilePageviewsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.MediaFilePageviewsResponse{Items: make([]entities.MediaFilePageviews, 0)}
	pathParams := []string{parameters.MediaFile, parameters.Wiki, convertToISO8601(parameters.Start), convertToISO8601(parameters.End)}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "pageviews_per_media_file_monthly", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.MediaFilePageviewsResponse{}
	}

	var dataGatewayResponse entities.DataGatewayMediaFilePageviewsResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.MediaFilePageviewsResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.MediaFilePageviewsResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return problemData, response
}
