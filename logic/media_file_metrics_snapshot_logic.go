package logic

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"encoding/json"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MediaFileMetricsSnapshotLogicInterface interface {
	ProcessMediaFileMetricsLogic(parameters entities.MediaFileMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFileMetricsSnapshotResponse)
}

type MediaFileMetricsSnapshotLogic struct {
	Config      *configuration.Config
	DataGateway DataGateway
}

func NewMediaFileMetricsSnapshotLogic(config *configuration.Config, datagateway DataGateway) MediaFileMetricsSnapshotLogic {
	return MediaFileMetricsSnapshotLogic{Config: config, DataGateway: datagateway}
}

func (s MediaFileMetricsSnapshotLogic) ProcessMediaFileMetricsLogic(parameters entities.MediaFileMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFileMetricsSnapshotResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.MediaFileMetricsSnapshotResponse{Items: make([]entities.MediaFileMetricsSnapshot, 0)}
	pathParams := []string{parameters.MediaFile, convertToISO8601(parameters.Start), convertToISO8601(parameters.End)}

	data, err := s.DataGateway.CallDataGateway(s.Config.DataGatewayURI, "media_file_metrics_snapshot", pathParams...)

	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.MediaFileMetricsSnapshotResponse{}
	}

	var dataGatewayResponse entities.DataGatewayMediaFileMetricsSnapshotResponse

	err = json.Unmarshal(data, &dataGatewayResponse)
	if err != nil {
		aqsassist.LogQueryFailed(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.MediaFileMetricsSnapshotResponse{}
	}
	if len(dataGatewayResponse.Rows) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.MediaFileMetricsSnapshotResponse{}
	}

	response.Items = dataGatewayResponse.Rows

	return problemData, response
}
