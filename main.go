package main

import (
	"commons-analytics/configuration"
	"commons-analytics/handlers"
	"commons-analytics/logic"
	"flag"
	"fmt"
	"net/http"
	"os"
	"path"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	prometheusmiddleware "github.com/albertogviana/prometheus-middleware"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

var (
	// These values are assigned at build using `-ldflags` (see: Makefile)
	buildDate      = "unknown"
	buildHost      = "unknown"
	version        = "unknown"
	dataGatewayURI string
)

// API documentation
// @title           Wikimedia Commons Impact Metrics API
// @version         1
// @description     Commons Impact Metrics provides analytical data on the impact of community
// @description     contributions to Wikimedia Commons, focused on contributions from galleries,
// @description     libraries, archives, and museums. Data provided by this API is available
// @description     under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).
// @termsOfService  https://foundation.wikimedia.org/wiki/Special:MyLanguage/Policy:Terms_of_Use
// @host            wikimedia.org
// @basePath        /api/rest_v1/metrics
// @schemes         https

// Entrypoint for the service
func main() {
	var confFile = flag.String("config", "./config.yaml", aqsassist.ConfigurationPathMessage)

	var config *configuration.Config
	var err error
	var dataGateway logic.DefaultDataGateway

	flag.Parse()

	if config, err = configuration.ReadConfig(*confFile); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	var categoryMetricLogic = logic.NewCategoryMetricLogic(config, dataGateway)
	var categoryEditsLogic = logic.NewCategoryEditsLogic(config, dataGateway)
	var mediaFileMetricsSnapshotLogic = logic.NewMediaFileMetricsSnapshotLogic(config, dataGateway)
	var categoryPageviewsLogic = logic.NewCategoryPageviewsLogic(config, dataGateway)
	var topPagesByCategoryLogic = logic.NewTopPagesByCategoryLogic(config, dataGateway)
	var userEditsLogic = logic.NewUserEditsLogic(config, dataGateway)
	var mediaFilePageviewsLogic = logic.NewMediaFilePageviewsLogic(config, dataGateway)
	var topWikisByCategoryLogic = logic.NewTopWikisByCategoryLogic(config, dataGateway)
	var topPagesByMediaFileLogic = logic.NewTopPagesByMediaFileLogic(config, dataGateway)
	var topViewedCategoriesLogic = logic.NewTopViewedCategoriesLogic(config, dataGateway)
	var topWikisByMediaFileLogic = logic.NewTopWikisByMediaFileLogic(config, dataGateway)
	var topViewedMediaFilesLogic = logic.NewTopViewedMediaFilesLogic(config, dataGateway)
	var topEditedCategoriesLogic = logic.NewTopEditedCategoriesLogic(config, dataGateway)
	var topEditorsLogic = logic.NewTopEditorsLogic(config, dataGateway)

	logger, err := log.NewLogger(os.Stdout, config.ServiceName, config.LogLevel)
	if err != nil {
		fmt.Fprintf(os.Stderr, aqsassist.ErrorInitializingLoggerMessage, err)
		os.Exit(1)
	}

	aqsassist.LogServiceInitialized(logger, config.ServiceName, version, buildHost, buildDate)

	// pass bound struct method to handler
	categoryMetricSnapshotHandler := &handlers.CategoryMetricSnapshotHandler{
		Logger: logger, Logic: &categoryMetricLogic, Config: config}
	categoryPageviewsHandler := &handlers.CategoryPageviewsHandler{
		Logger: logger, Logic: &categoryPageviewsLogic, Config: config}
	mediafileMetricsSnapshotHandler := &handlers.MediafileMetricsSnapshotHandler{
		Logger: logger, Logic: &mediaFileMetricsSnapshotLogic, Config: config}
	mediaFilePageviewsHandler := &handlers.MediaFilePageviewsHandler{
		Logger: logger, Logic: &mediaFilePageviewsLogic, Config: config}
	categoryEditsHandler := &handlers.CategoryEditsHandler{
		Logger: logger, Logic: &categoryEditsLogic, Config: config}
	userEditsHandler := &handlers.UserEditsHandler{
		Logger: logger, Logic: &userEditsLogic, Config: config}
	topPagesByCategoryHandler := &handlers.TopPagesByCategoryHandler{
		Logger: logger, Logic: &topPagesByCategoryLogic, Config: config}
	topWikisByCategoryHandler := &handlers.TopWikisByCategoryHandler{
		Logger: logger, Logic: &topWikisByCategoryLogic, Config: config}
	topViewedCategoriesHandler := &handlers.TopViewedCategoriesHandler{
		Logger: logger, Logic: &topViewedCategoriesLogic, Config: config}
	topPagesByMediaFileHandler := &handlers.TopPagesByMediaFileHandler{
		Logger: logger, Logic: &topPagesByMediaFileLogic, Config: config}
	topWikisByMediaFileHandler := &handlers.TopWikisByMediaFileHandler{
		Logger: logger, Logic: &topWikisByMediaFileLogic, Config: config}
	topViewedMediaFilesHandler := &handlers.TopViewedMediaFilesHandler{
		Logger: logger, Logic: &topViewedMediaFilesLogic, Config: config}
	topEditedCategoriesHandler := &handlers.TopEditedCategoriesHandler{
		Logger: logger, Logic: &topEditedCategoriesLogic, Config: config}
	topEditorsHandler := &handlers.TopEditorsHandler{
		Logger: logger, Logic: &topEditorsLogic, Config: config}
	apiSpecHandler := &handlers.ApiSpecHandler{
		Logger: logger}

	healthz := NewHealthz(version, buildDate, buildHost)
	middleware := prometheusmiddleware.NewPrometheusMiddleware(prometheusmiddleware.Opts{})

	r := mux.NewRouter().SkipClean(true).UseEncodedPath()
	r.NotFoundHandler = http.HandlerFunc(handlers.NotFoundHandler)
	p := promhttp.Handler()

	r.Use(SetSecurityHeaders, ValidateParameters, middleware.InstrumentHandlerDuration)

	r.Handle("/metrics", p).Methods("GET")
	r.HandleFunc("/healthz", healthz.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/category-metrics-snapshot/{category}/{start}/{end}"), categoryMetricSnapshotHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/media-file-metrics-snapshot/{media-file}/{start}/{end}"), mediafileMetricsSnapshotHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/pageviews-per-category-monthly/{category}/{category-scope}/{wiki}/{start}/{end}"), categoryPageviewsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/pageviews-per-media-file-monthly/{media-file}/{wiki}/{start}/{end}"), mediaFilePageviewsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/edits-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end}"), categoryEditsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/edits-per-user-monthly/{user-name}/{edit-type}/{start}/{end}"), userEditsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-pages-per-category-monthly/{category}/{category-scope}/{wiki}/{year}/{month}"), topPagesByCategoryHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month}"), topWikisByCategoryHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month}"), topViewedCategoriesHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month}"), topPagesByMediaFileHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-wikis-per-media-file-monthly/{media-file}/{year}/{month}"), topWikisByMediaFileHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-viewed-media-files-monthly/{category}/{category-scope}/{wiki}/{year}/{month}"), topViewedMediaFilesHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-editors-monthly/{category}/{category-scope}/{edit-type}/{year}/{month}"), topEditorsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-edited-categories-monthly/{category-scope}/{edit-type}/{year}/{month}"), topEditedCategoriesHandler.HandleHTTP).Methods("GET")
	r.HandleFunc("/commons-impact-analytics/api-spec.json", apiSpecHandler.HandleHTTP).Methods("GET")

	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.Address, config.Port),
		Handler: r,
	}

	err = srv.ListenAndServe()
	fmt.Println(err.Error())
}
