package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopWikisByCategoryHandler struct {
	Logger *logger.Logger
	Logic  logic.TopWikisByCategoryLogicInterface
	Config *configuration.Config
}

// API documentation
//
//		@summary		Get ranking of wikis with most pageviews for a given category
//		@router			/commons-analytics/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month} [get]
//		@description	Returns the ranking of wikis with the most viewed pages containing media files from the given Commons category or category tree.
//	    @param			category		path	string	true	"The title of the desired category, in URL format"	                        example(Gallica)
//		@param			category-scope	path	string	true	"Plain category counts (shallow) or top-level category tree counts (deep)"	example(deep)     enums(shallow, deep)
//		@param			year		    path	string	true	"Year, in YYYY format"	                                                    example(2024)
//		@param			month			path	string	true	"Month, in MM format"	                                                    example(05)
//		@produce		json
//		@success		200	{object}	entities.TopWikisByCategoryResponse
//		@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//		@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//		@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopWikisByCategoryHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	year := vars["year"]
	month := vars["month"]
	category := vars["category"]
	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopWikisByCategoryParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopWikisByCategoryLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopWikisByCategoryContext{
		Endpoint:      "commons-analytics/top-wikis-per-category-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
