package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopEditorsHandler struct {
	Logger *logger.Logger
	Logic  logic.TopEditorsLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	@summary		Get ranking of users with most edits to a given category
//	@router			/commons-analytics/top-editors-monthly/{category}/{category-scope}/{edit-type}/{year}/{month} [get]
//	@description	Returns the ranking of users with most edits to Commons media files associated to the given category or category tree.
//	@param			category		    path	string	true	"The title of the desired category, in URL format"	                        example(Gallica)
//	@param			category-scope		path	string	true	"Plain category counts (shallow) or top-level category tree counts (deep)"	example(deep)     enums(shallow, deep)
//	@param			edit-type		    path	string	true	"The type of edit: create, update or all-edit-types"	                    example(create)   enums(create, update, all-edit-types)
//	@param			year		        path	string	true	"Year, in YYYY format"	                                                    example(2024)
//	@param			month			    path	string	true	"Month, in MM format"	                                                    example(05)
//	@produce		json
//	@success		200	{object}	entities.TopEditorsResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopEditorsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	year := vars["year"]
	month := vars["month"]
	category := vars["category"]
	var err error

	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]
	editType := vars["edit-type"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsEditValue(editType) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidEditTypeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopEditorsParameters{
		Category:      category,
		CategoryScope: categoryScope,
		EditType:      editType,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopEditorsLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopEditorsContext{
		Endpoint:      "commons-analytics/top-editors-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		EditType:      editType,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
