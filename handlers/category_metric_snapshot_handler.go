package handlers

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryMetricSnapshotHandler struct {
	Logger *logger.Logger
	Logic  logic.CategoryMetricLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	@summary		Get time series of category metrics
//	@router			/commons-analytics/category-metrics-snapshot/{category}/{start}/{end}  [get]
//	@description	Returns a time series with metrics about the given Commons category and category tree, including media file counts and leverage counts.
//	@param			category	path	string	true	"The title of the desired category, in URL format"	    example(Gallica)
//	@param			start		path	string	true	"First month (inclusive), in YYYYMM01 format"	        example(20240101)
//	@param			end			path	string	true	"Last month (exclusive), in YYYYMM01 format"	        example(20240501)
//	@produce		json
//	@success		200	{object}	entities.CategoryMetricResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *CategoryMetricSnapshotHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	category := vars["category"]
	start := strings.ToLower(vars["start"])
	end := strings.ToLower(vars["end"])

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	var err error

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.CategoryMetricsSnapshotParameters{
		Category: category,
		Start:    start,
		End:      end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessCategoryMetricLogic(params, callContext)

	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.CategoryMetricsSnapshotContext{
		Endpoint: "commons-analytics/category-metrics-snapshot",
		Category: category,
		Start:    start,
		End:      end,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
