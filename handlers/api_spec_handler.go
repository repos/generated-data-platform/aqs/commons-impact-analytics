package handlers

import (
	"io/ioutil"
	"net/http"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
)

// ApiSpecHandler is the HTTP handler for serving the swagger (OpenAPI) spec
type ApiSpecHandler struct {
	Logger *logger.Logger
}

func (s *ApiSpecHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	// When executed via go test, the working directory is relative to the test binary,
	// so a swaggerFilePath like "./docs/swagger.json" will not find the swagger spec
	// and handler unit tests will fail. Instead, we build the path relative to this handler,
	// which will always be in a consistent place relative to the swagger spec file.
	_, handlerFilePath, _, ok := runtime.Caller(0)
	if !ok {
		handlerFilePathErrMsg := aqsassist.FailedToCreateHandlerPathErrorMessage
		s.Logger.Error(handlerFilePathErrMsg)
		http.Error(w, handlerFilePathErrMsg, http.StatusInternalServerError)
		return
	}
	handlerDir := strings.TrimRight(filepath.Dir(handlerFilePath), "/")
	docPath := handlerDir[0:strings.LastIndex(handlerDir, "/")]
	swaggerFilePath := docPath + aqsassist.SwaggerDocPath

	swaggerFile, err := ioutil.ReadFile(swaggerFilePath)
	if err != nil {
		s.Logger.Error(aqsassist.SwaggerFileLoadingErrorMessage, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(swaggerFile)
}
