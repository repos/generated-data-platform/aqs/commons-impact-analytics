package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopViewedCategoriesHandler struct {
	Logger *logger.Logger
	Logic  logic.TopViewedCategoriesLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	@summary		Get ranking of categories with most pageviews
//	@router			/commons-analytics/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month} [get]
//	@description	Returns the ranking of Commons categories or category trees whose associated media files appear in the wiki pages with most pageviews.
//	@param			category-scope	path	string	true	"Plain category counts (shallow) or top-level category tree counts (deep)"	    example(shallow)    enums(shallow, deep)
//	@param			wiki		    path	string	true	"The wiki to show metrics for. Use 'all-wikis' for an overall aggregate count"	example(all-wikis)
//	@param			year		    path	string	true	"Year, in YYYY format"	                                                        example(2024)
//	@param			month			path	string	true	"Month, in MM format"	                                                        example(05)
//	@produce		json
//	@success		200	{object}	entities.TopViewedCategoriesResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopViewedCategoriesHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	year := vars["year"]
	month := vars["month"]
	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]
	wiki := vars["wiki"]
	var err error

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopViewedCategoriesParameters{
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopViewedCategoriesLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopViewedCategoriesContext{
		Endpoint:      "commons-analytics/top-viewed-categories-monthly",
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
