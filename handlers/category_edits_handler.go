package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryEditsHandler struct {
	Logger *logger.Logger
	Logic  logic.CategoryEditsLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	@summary		Get time series of edit counts for a given category
//	@router			/commons-analytics/edits-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end} [get]
//	@description	Returns a time series of the number of edits to Commons media files associated to the given category or category tree.
//	@param			category       path	string	true	"The title of the desired category, in URL format"	                       example(Gallica)
//	@param			category-scope path	string	true	"Plain category counts (shallow) or top-level category tree counts (deep)" example(deep)      enums(shallow, deep)
//	@param			edit-type      path	string	true	"The type of edit: create, update or all-edit-types"	                   example(create)    enums(create, update, all-edit-types)
//	@param			start		   path	string	true	"First month (inclusive), in YYYYMM01 format"	                           example(20240101)
//	@param			end			   path	string	true	"Last month (exclusive), in YYYYMM01 format"	                           example(20240501)
//	@produce		json
//	@success		200	{object}	entities.CategoryEditsResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *CategoryEditsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	start := vars["start"]
	end := vars["end"]
	category := vars["category"]
	categoryScope := vars["category-scope"]
	editType := vars["edit-type"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsEditValue(editType) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidEditTypeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.CategoryEditsParameters{
		Category:      category,
		CategoryScope: categoryScope,
		EditType:      editType,
		Start:         start,
		End:           end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessCategoryEditsLogic(params, callContext)

	response.Context = entities.CategoryEditsContext{
		Endpoint:      "commons-analytics/edits-per-category-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		EditType:      editType,
		Start:         start,
		End:           end,
	}

	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
