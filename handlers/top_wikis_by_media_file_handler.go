package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopWikisByMediaFileHandler struct {
	Logger *logger.Logger
	Logic  logic.TopWikisByMediaFileLogicInterface
	Config *configuration.Config
}

// API documentation
//
//		@summary		Get ranking of wikis with most pageviews for a given media file
//		@router			/commons-analytics/top-wikis-per-media-file-monthly/{media-file}/{year}/{month} [get]
//		@description	Returns the ranking of wikis with the most viewed pages containing the given Commons media file.
//	    @param			media-file		path	string	true	"The title of the desired media file, in URL format"	example(Flag_of_UNESCO.svg)
//		@param			year		    path	string	true	"Year, in YYYY format"	                                example(2024)
//		@param			month			path	string	true	"Month, in MM format"	                                example(05)
//		@produce		json
//		@success		200	{object}	entities.TopWikisByMediaFileResponse
//		@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//		@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//		@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopWikisByMediaFileHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	year := vars["year"]
	month := vars["month"]
	mediaFile := vars["media-file"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopWikisByMediaFileParameters{
		MediaFile: mediaFile,
		Year:      year,
		Month:     month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopWikisByMediaFileLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopWikisByMediaFileContext{
		Endpoint:  "commons-analytics/top-wikis-per-media-file-monthly",
		MediaFile: mediaFile,
		Year:      year,
		Month:     month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
