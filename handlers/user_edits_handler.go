package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type UserEditsHandler struct {
	Logger *logger.Logger
	Logic  logic.UserEditsLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	    @summary		Get time series of edit counts for a given user
//		@router			/commons-analytics/edits-per-user-monthly/{user-name}/{edit-type}/{start}/{end} [get]
//		@description	Returns a time series of the number of edits to Commons media files performed by the given user.
//		@param			user-name		path	string	true	"The username of the desired editor"	                example(SchlurcherBot)
//		@param			edit-type		path	string	true	"The type of edit: create, update or all-edit-types"	example(update)         enums(create, update, all-edit-types)
//		@param			start		    path	string	true	"First month (inclusive), in YYYYMM01 format"	        example(20240101)
//		@param			end			    path	string	true	"Last month (exclusive), in YYYYMM01 format"	        example(20240501)
//		@produce		json
//		@success		200	{object}	entities.UserEditsResponse
//		@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//		@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//		@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *UserEditsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	start := vars["start"]
	end := vars["end"]
	userName := vars["user-name"]
	editType := vars["edit-type"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsEditValue(editType) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidEditTypeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.UserEditsParameters{
		UserName: userName,
		EditType: editType,
		Start:    start,
		End:      end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessUserEditsLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.UserEditsContext{
		Endpoint: "commons-analytics/edits-per-user-monthly",
		UserName: userName,
		EditType: editType,
		Start:    start,
		End:      end,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
