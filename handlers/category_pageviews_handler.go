package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryPageviewsHandler struct {
	Logger *logger.Logger
	Logic  logic.CategoryPageviewsLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	@summary		Get time series of pageview counts for a given category
//	@router			/commons-analytics/pageviews-per-category-monthly/{category}/{category-scope}/{wiki}/{start}/{end} [get]
//	@description	Returns a time series of the number of pageviews to Commons media files associated to the given category or category tree.
//	@param			category	   path	 string	true	"The title of the desired category, in URL format"	                            example(Gallica)
//	@param			category-scope path	 string	true	"Plain category counts (shallow) or top-level category tree counts (deep)"	    example(deep)          enums(shallow, deep)
//	@param			wiki     	   path	 string	true	"The wiki to show metrics for. Use 'all-wikis' for an overall aggregate count"	example(en.wikipedia)
//	@param			start		   path	 string	true	"First month (inclusive), in YYYYMM01 format"	                                example(20240101)
//	@param			end			   path	 string	true	"Last month (exclusive), in YYYYMM01 format"	                                example(20240501)
//	@produce		json
//	@success		200	{object}	entities.CategoryPageviewsResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *CategoryPageviewsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	start := vars["start"]
	end := vars["end"]
	category := vars["category"]
	categoryScope := vars["category-scope"]
	wiki := vars["wiki"]
	// NOTE: removed pageTitle as an input per discussion last week, here and everywhere

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.CategoryPageviewsParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Start:         start,
		End:           end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessCategoryPageviewsLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.CategoryPageviewsContext{
		Endpoint:      "commons-analytics/pageviews-per-category-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Start:         start,
		End:           end,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
