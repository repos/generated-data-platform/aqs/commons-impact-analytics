package handlers

import (
	"commons-analytics/configuration"
	"commons-analytics/entities"
	"commons-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopPagesByMediaFileHandler struct {
	Logger *logger.Logger
	Logic  logic.TopPagesByMediaFileLogicInterface
	Config *configuration.Config
}

// API documentation
//
//	@summary		Get ranking of pages with most pageviews for a given media file
//	@router			/commons-analytics/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month} [get]
//	@description	Returns the ranking of the most visited wiki pages containing the given media file.
//	@param			media-file		    path	string	true	"The title of the desired media-file, in URL format"	                        example(Flag_of_UNESCO.svg)
//	@param			wiki		        path	string	true	"The wiki to show metrics for. Use 'all-wikis' for an overall aggregate count"	example(en.wikipedia)
//	@param			year			    path	string	true	"Year, in YYYY format"	                                                        example(2024)
//	@param			month			    path	string	true	"Month, in MM format"	                                                        example(05)
//	@produce		json
//	@success		200	{object}	entities.TopPagesByMediaFileResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopPagesByMediaFileHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	year := vars["year"]
	month := vars["month"]
	mediaFile := vars["media-file"]
	wiki := vars["wiki"]
	var err error

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopPagesByMediaFileParameters{
		MediaFile: mediaFile,
		Wiki:      wiki,
		Year:      year,
		Month:     month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopPagesByMediaFileLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopPagesByMediaFileContext{
		Endpoint:  "commons-analytics/top-pages-per-media-file-monthly",
		MediaFile: mediaFile,
		Wiki:      wiki,
		Year:      year,
		Month:     month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
