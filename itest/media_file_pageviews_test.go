package itest

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"testing"

	"commons-analytics/entities"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

func TestMediaFilPageviews(t *testing.T) {

	t.Run("should return 200 with all-wikis", func(t *testing.T) {
		res, err := http.Get(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/20221101/20231201"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := entities.CategoryEditsResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.Len(t, obj.Items, 1, "Invalid number of results")
	})

	t.Run("should return 400 when invalid timestamp", func(t *testing.T) {
		res, err := http.Get(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/2022110s1/20231201"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 400 when start is after end date", func(t *testing.T) {
		res, err := http.Get(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/20231204/20231201"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 404 when matching timepstamps", func(t *testing.T) {
		res, err := http.Get(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/20231101/20231101"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusNotFound, res.StatusCode, "Incorrect HTTP status code")
	})

	t.Run("should return 404 when no data found", func(t *testing.T) {
		res, err := http.Get(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/19890202/20001201"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusNotFound, res.StatusCode, "Incorrect HTTP status code")
	})

	t.Run("should return 405 for invalid HTTP verb", func(t *testing.T) {
		jsonBody := []byte(``)
		bodyReader := bytes.NewReader(jsonBody)
		res, err := http.Post(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/20221101/20231201"), "application/json; charset=utf-8", bodyReader)
		require.NoError(t, err, "Invalid http request")
		require.Equal(t, http.StatusMethodNotAllowed, res.StatusCode, "Wrong status code")
	})

	t.Run("All security headers should be properly set", func(t *testing.T) {
		res, _ := http.Get(apiTestURL("pageviews-per-media-file-monthly/Airavatesvara_Temple_wall_details.JPG/all-wikis/20221101/20231201"))
		for headerName, headerValue := range aqsassist.HeadersToAdd {
			assert.Equal(t, headerValue, res.Header.Get(headerName), headerName+" should be set to "+headerValue)
		}
		assert.NotEmpty(t, res.Header.Get("Etag"), "Etag should be set")
	})
}
