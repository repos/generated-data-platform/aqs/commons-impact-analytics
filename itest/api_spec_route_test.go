package itest

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestValidRoute(t *testing.T) {
	t.Run("should return 200 for api spec route", func(t *testing.T) {
		var specUrl = "http://localhost:8080/commons-impact-analytics/api-spec.json"
		res, err := http.Get(specUrl)

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")
	})
}

func TestInvalidRoute(t *testing.T) {
	t.Run("should return 404 in case of invalid route", func(t *testing.T) {

		res, err := http.Get(apiTestURL("analytics/api-spec"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusNotFound, res.StatusCode, "Wrong status code")

	})
}
