package itest

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"testing"

	"commons-analytics/entities"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

// httperr is a (minimal) RFC 7807 problem object
type httperr struct {
	Title  string `json:"title"`
	Detail string `json:"detail"`
}

// Returns the value of the API_URL environment variable -if set- or a reasonable
// default otherwise. The supplied suffix value is appended to the resulting URL.
func apiTestURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/commons-analytics"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}

func TestCategoryEdits(t *testing.T) {

	t.Run("should return 200 with shallow category scope", func(t *testing.T) {
		res, err := http.Get(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20221101/20231201"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := entities.CategoryEditsResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.Len(t, obj.Items, 1, "Invalid number of results")
	})

	t.Run("should return 400 when invalid timestamp", func(t *testing.T) {
		res, err := http.Get(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20221101/201901a2"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 400 when start date is after end date", func(t *testing.T) {
		res, err := http.Get(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20221101/2019012"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 400 when invalid edit type", func(t *testing.T) {
		res, err := http.Get(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/invalid-types/20221101/20231201"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should not return valid data when matching timepstamps", func(t *testing.T) {
		res, err := http.Get(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20231101/20231101"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusNotFound, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 404 when no data found", func(t *testing.T) {
		res, err := http.Get(apiTestURL("edits-per-category-monthly/974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20221101/20221101"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusNotFound, res.StatusCode, "Incorrect HTTP status code")
	})

	t.Run("should return 405 for invalid HTTP verb", func(t *testing.T) {
		jsonBody := []byte(``)
		bodyReader := bytes.NewReader(jsonBody)
		res, err := http.Post(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20221101/20221101"), "application/json; charset=utf-8", bodyReader)
		require.NoError(t, err, "Invalid http request")
		require.Equal(t, http.StatusMethodNotAllowed, res.StatusCode, "Wrong status code")
	})

	t.Run("All security headers should be properly set", func(t *testing.T) {
		res, _ := http.Get(apiTestURL("edits-per-category-monthly/1974_State_Visit_to_the_USSR_photo_kit_at_the_Gerald_R._Ford_Presidential_Library/shallow/all-edit-types/20221101/20231201"))
		for headerName, headerValue := range aqsassist.HeadersToAdd {
			assert.Equal(t, headerValue, res.Header.Get(headerName), headerName+" should be set to "+headerValue)
		}
		assert.NotEmpty(t, res.Header.Get("Etag"), "Etag should be set")
	})

}
