package itest

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"testing"

	"commons-analytics/entities"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

func TestTopEditedCategories(t *testing.T) {

	t.Run("should return 200 with shallow category scope", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-edited-categories-monthly/shallow/create/2023/11"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := entities.CategoryEditsResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.Len(t, obj.Items, 89, "Invalid number of results")
	})

	t.Run("should return 400 when invalid year", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-edited-categories-monthly/shallow/create/202ee3/11"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 400 when invalid edit type", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-edited-categories-monthly/shallow/invalid/2023/11"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 400 when invalid category scope", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-edited-categories-monthly/invalid/create/2023/11"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 400 when month is invalid", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-edited-categories-monthly/shallow/create/2023/111"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		body, err := io.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("should return 404 when no data found", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-edited-categories-monthly/shallow/create/1970/11"))
		require.NoError(t, err, "HTTP request error")
		require.Equal(t, http.StatusNotFound, res.StatusCode, "Incorrect HTTP status code")
	})

	t.Run("should return 405 for invalid HTTP verb", func(t *testing.T) {
		jsonBody := []byte(``)
		bodyReader := bytes.NewReader(jsonBody)
		res, err := http.Post(apiTestURL("top-edited-categories-monthly/shallow/create/2023/11"), "application/json; charset=utf-8", bodyReader)
		require.NoError(t, err, "Invalid http request")
		require.Equal(t, http.StatusMethodNotAllowed, res.StatusCode, "Wrong status code")
	})

	t.Run("All security headers should be properly set", func(t *testing.T) {
		res, _ := http.Get(apiTestURL("top-edited-categories-monthly/shallow/create/2023/11"))
		for headerName, headerValue := range aqsassist.HeadersToAdd {
			assert.Equal(t, headerValue, res.Header.Get(headerName), headerName+" should be set to "+headerValue)
		}
		assert.NotEmpty(t, res.Header.Get("Etag"), "Etag should be set")
	})
}
