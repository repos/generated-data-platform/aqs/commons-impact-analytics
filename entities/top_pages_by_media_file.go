package entities

type TopPagesByMediaFileParameters struct {
	MediaFile string
	Wiki      string
	Year      string
	Month     string
}

type TopPagesByMediaFileContext struct {
	Endpoint  string `json:"endpoint" example:"commons-analytics/top-pages-per-media-file-monthly"`
	MediaFile string `json:"media-file" example:"Flag_of_UNESCO.svg"`
	Wiki      string `json:"wiki" example:"en.wikipedia"`
	Year      string `json:"year" example:"2024"`
	Month     string `json:"month" example:"05"`
}

type TopPagesByMediaFileResponse struct {
	Context TopPagesByMediaFileContext `json:"context"`
	Items   []TopPagesByMediaFile      `json:"items"`
}

type TopPagesByMediaFile struct {
	PageTitle     string `json:"page-title" example:"Bangkok"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}

type DataGatewayTopPagesByMediaFileResponse struct {
	Rows []TopPagesByMediaFile `json:"rows"`
}
