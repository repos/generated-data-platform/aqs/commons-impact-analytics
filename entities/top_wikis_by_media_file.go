package entities

type TopWikisByMediaFileParameters struct {
	MediaFile string
	Year      string
	Month     string
}

type TopWikisByMediaFileContext struct {
	Endpoint  string `json:"endpoint" example:"commons-analytics/top-wikis-per-media-file-monthly"`
	MediaFile string `json:"media-file" example:"Flag_of_UNESCO.svg"`
	Year      string `json:"year" example:"2024"`
	Month     string `json:"month" example:"05"`
}

type TopWikisByMediaFileResponse struct {
	Context TopWikisByMediaFileContext `json:"context"`
	Items   []TopWikisByMediaFile      `json:"items"`
}

type TopWikisByMediaFile struct {
	Wiki          string `json:"wiki" example:"en.wikipedia"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}

type DataGatewayTopWikisByMediaFileResponse struct {
	Rows []TopWikisByMediaFile `json:"rows"`
}
