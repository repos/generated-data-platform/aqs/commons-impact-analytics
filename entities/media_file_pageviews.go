package entities

type MediaFilePageviewsParameters struct {
	MediaFile string
	Wiki      string
	Start     string
	End       string
}

type MediaFilePageviewsContext struct {
	Endpoint  string `json:"endpoint" example:"commons-analytics/pageviews-per-media-file-monthly"`
	MediaFile string `json:"media-file" example:"Flag_of_UNESCO.svg"`
	Wiki      string `json:"wiki" example:"en.wikipedia"`
	Start     string `json:"start" example:"2024010100"`
	End       string `json:"end" example:"2024050100"`
}

type MediaFilePageviewsResponse struct {
	Context MediaFilePageviewsContext `json:"context"`
	Items   []MediaFilePageviews      `json:"items"`
}

type MediaFilePageviews struct {
	Timestamp     string `json:"timestamp" example:"2024010100"`
	PageviewCount int    `json:"pageview-count" example:"47"`
}

type DataGatewayMediaFilePageviewsResponse struct {
	Rows []MediaFilePageviews `json:"rows"`
}
