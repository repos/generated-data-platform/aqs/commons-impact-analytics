package entities

type TopEditedCategoriesParameters struct {
	CategoryScope string
	EditType      string
	Year          string
	Month         string
}

type TopEditedCategoriesContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/top-edited-categories-monthly"`
	CategoryScope string `json:"category-scope" example:"shallow"`
	EditType      string `json:"edit-type" example:"create"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"05"`
}

type TopEditedCategoriesResponse struct {
	Context TopEditedCategoriesContext `json:"context"`
	Items   []TopEditedCategories      `json:"items"`
}

type TopEditedCategories struct {
	Category  string `json:"category" example:"Gallica"`
	EditCount int    `json:"edit-count" example:"47"`
	Rank      int    `json:"rank" example:"4"`
}

type DataGatewayTopEditedCategoriesResponse struct {
	Rows []TopEditedCategories `json:"rows"`
}
