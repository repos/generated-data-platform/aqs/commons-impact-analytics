package entities

type CategoryEditsParameters struct {
	Category      string
	CategoryScope string
	EditType      string
	Start         string
	End           string
}

type CategoryEditsContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/edits-per-category-monthly"`
	Category      string `json:"category" example:"Gallica"`
	CategoryScope string `json:"category-scope" example:"deep"`
	EditType      string `json:"edit-type" example:"create"`
	Start         string `json:"start" example:"2024010100"`
	End           string `json:"end" example:"2024050100"`
}

type CategoryEditsResponse struct {
	Context CategoryEditsContext `json:"context"`
	Items   []CategoryEdits      `json:"items"`
}

type CategoryEdits struct {
	Timestamp string `json:"timestamp" example:"2024010100"`
	EditCount int    `json:"edit-count" example:"47"`
}

type DataGatewayCategoryEditsResponse struct {
	Rows []CategoryEdits `json:"rows"`
}
