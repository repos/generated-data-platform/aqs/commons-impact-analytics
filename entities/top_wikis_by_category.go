package entities

type TopWikisByCategoryParameters struct {
	Category      string
	CategoryScope string
	Year          string
	Month         string
}

type TopWikisByCategoryContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/top-wikis-per-category-monthly"`
	Category      string `json:"category" example:"Gallica"`
	CategoryScope string `json:"category-scope" example:"deep"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"05"`
}

type TopWikisByCategoryResponse struct {
	Context TopWikisByCategoryContext `json:"context"`
	Items   []TopWikisByCategory      `json:"items"`
}

type TopWikisByCategory struct {
	Wiki          string `json:"wiki" example:"en.wikipedia"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}

type DataGatewayTopWikisByCategoryResponse struct {
	Rows []TopWikisByCategory `json:"rows"`
}
