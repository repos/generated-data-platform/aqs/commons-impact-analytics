package entities

type TopViewedMediaFilesParameters struct {
	Category      string
	CategoryScope string
	Wiki          string
	Year          string
	Month         string
}

type TopViewedMediaFilesContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/top-viewed-media-files-monthly"`
	Category      string `json:"category" example:"Gallica"`
	CategoryScope string `json:"category-scope" example:"deep"`
	Wiki          string `json:"wiki" example:"all-wikis"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"05"`
}

type TopViewedMediaFilesResponse struct {
	Context TopViewedMediaFilesContext `json:"context"`
	Items   []TopViewedMediaFiles      `json:"items"`
}

type TopViewedMediaFiles struct {
	MediaFile     string `json:"media-file" example:"Habits_gaulois.jpg"`
	PageviewCount int    `json:"pageview-count" example:"46"`
	Rank          int    `json:"rank" example:"4"`
}

type DataGatewayTopViewedMediaFilesResponse struct {
	Rows []TopViewedMediaFiles `json:"rows"`
}
