package entities

type TopEditorsParameters struct {
	Category      string
	CategoryScope string
	EditType      string
	Year          string
	Month         string
}

type TopEditorsContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/top-editors-monthly"`
	Category      string `json:"category" example:"Gallica"`
	CategoryScope string `json:"category-scope" example:"deep"`
	EditType      string `json:"edit-type" example:"create"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"05"`
}

type TopEditorsResponse struct {
	Context TopEditorsContext `json:"context"`
	Items   []TopEditors      `json:"items"`
}

type TopEditors struct {
	UserName  string `json:"user-name" example:"SchlurcherBot"`
	EditCount int    `json:"edit-count" example:"47"`
	Rank      int    `json:"rank" example:"4"`
}

type DataGatewayTopEditorsResponse struct {
	Rows []TopEditors `json:"rows"`
}
