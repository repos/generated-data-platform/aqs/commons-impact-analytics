package entities

type CategoryPageviewsParameters struct {
	Category      string
	CategoryScope string
	Wiki          string
	Start         string
	End           string
}

type CategoryPageviewsContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/pageviews-per-category-monthly"`
	Category      string `json:"category" example:"Gallica"`
	CategoryScope string `json:"category-scope" example:"deep"`
	Wiki          string `json:"wiki" example:"en.wikipedia"`
	Start         string `json:"start" example:"2024010100"`
	End           string `json:"end" example:"2024050100"`
}

type CategoryPageviewsResponse struct {
	Context CategoryPageviewsContext `json:"context"`
	Items   []CategoryPageviews      `json:"items"`
}

type CategoryPageviews struct {
	Timestamp     string `json:"timestamp" example:"2024010100"`
	PageviewCount int    `json:"pageview-count" example:"47"`
}

type DataGatewayCategoryPageviewsResponse struct {
	Rows []CategoryPageviews `json:"rows"`
}
