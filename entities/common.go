package entities

import (
	"context"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
)

type CallContext struct {
	Context    context.Context
	Logger     *logger.RequestScopedLogger
	RequestURL string
}
