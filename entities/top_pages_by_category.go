package entities

type TopPagesByCategoryParameters struct {
	Category      string
	CategoryScope string
	Wiki          string
	Year          string
	Month         string
}

type TopPagesByCategoryContext struct {
	Endpoint      string `json:"endpoint" example:"commons-analytics/top-pages-per-category-monthly"`
	Category      string `json:"category" example:"Gallica"`
	CategoryScope string `json:"category-scope" example:"deep"`
	Wiki          string `json:"wiki" example:"en.wikipedia"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"05"`
}

// For top endpoints, data is stored in json and parsed by the logic layer
// NOTE: this is different from previous AQS "tops" endpoints where,
// for example, "countries" was nested within the first item in "items" TODO: confirm this is intended
type TopPagesByCategoryResponse struct {
	Context TopPagesByCategoryContext `json:"context"`
	Items   []TopPagesByCategory      `json:"items"`
}

type TopPagesByCategory struct {
	PageTitle     string `json:"page-title" example:"Bangkok"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}

type DataGatewayTopPagesByCategoryResponse struct {
	Rows []TopPagesByCategory `json:"rows"`
}
